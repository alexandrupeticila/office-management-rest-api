package com.example.office.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {CustomException.class})
    private ResponseEntity<String> handleCustomException(CustomException exception) {
        log.error(exception.getMessage() + LocalDateTime.now());
        return new ResponseEntity<>(exception.getMessage(), exception.getHttpStatus());
    }

}
