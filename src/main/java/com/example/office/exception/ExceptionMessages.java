package com.example.office.exception;

public enum ExceptionMessages {
    USER_ALREADY_EXISTS("User already exists"),
    NO_USER_FOUND("User with provided email was not found"),
    NO_DESK_AVAILABLE("No desk available for "),
    ALREADY_HAS_RESERVATION("You already have a reservation on: "),
    NO_RESERVATION_FOR_DATE("You don't have a reservation on: ");

    private final String message;

    ExceptionMessages(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
