package com.example.office.Util;

import com.example.office.entity.User;
import com.example.office.model.request.UserModelRequest;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserMapper {

    private final ModelMapper modelMapper;

    public User mapModelRequestToEntity(UserModelRequest userModelRequest){
        return modelMapper.map(userModelRequest, User.class);
    }

}
