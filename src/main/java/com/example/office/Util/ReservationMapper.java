package com.example.office.Util;

import com.example.office.entity.Reservation;
import com.example.office.entity.User;
import com.example.office.model.response.ReservationRest;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ReservationMapper {
    private final ModelMapper mapper;

    public ReservationRest convertToRest(Reservation reservation) {
        ReservationRest reservationRest = mapper.map(reservation, ReservationRest.class);
        User user = reservation.getUser();
        reservationRest.setUserEmail(user.getEmail());
        reservationRest.setUserFirstName(user.getFirstName());
        reservationRest.setUserLastName(user.getLastName());
        return reservationRest;
    }
}
