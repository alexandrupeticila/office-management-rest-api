package com.example.office.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Properties {

    public static Integer desksNumber;

    @Value("#{new Integer('${office.desksNumber}')}")
    public void setPrivateDesksNumber(Integer privateDesksNumber){
        Properties.desksNumber = privateDesksNumber;
    }
}
