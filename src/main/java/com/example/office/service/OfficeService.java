package com.example.office.service;

import com.example.office.configuration.Properties;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class OfficeService {

    private final Integer taskTime = 500;
    private final Integer desksNumber = Properties.desksNumber;
    private final ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10);

    public Integer getDesksNumber(){
        return desksNumber;
    }
    @SneakyThrows
    public long doCleaning(Integer workersNumber) {
        LocalTime startTime = LocalTime.now();
        ExecutorService executorService = Executors.newFixedThreadPool(workersNumber);
        for (int i = 0; i < desksNumber; i++) {
            executorService.submit(this::performCleaning);
        }
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        LocalTime endTime = LocalTime.now();
        return ChronoUnit.MILLIS.between(startTime, endTime);
    }

    @SneakyThrows
    public long splitCleaning() {
        LocalTime startTime = LocalTime.now();
        Thread cleanDustThread = new Thread(getCleanDustTask());
        Thread sanitizeThread = new Thread(getSanitizeTask());
        cleanDustThread.start();
        sanitizeThread.start();
        cleanDustThread.join();
        sanitizeThread.join();
        LocalTime endTime = LocalTime.now();
        return ChronoUnit.MILLIS.between(startTime, endTime);
    }

    private Runnable getCleanDustTask() {
        return () -> {
            for (int i = 0; i < desksNumber; i++) {
                cleanDust();
                try {
                    queue.put(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private Runnable getSanitizeTask() {
        return () -> {
            for (int i = 0; i < desksNumber; i++) {
                try {
                    Integer deskNumber = queue.take();
                    log.info("Desk with number: " + deskNumber);
                    sanitize();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private void performCleaning() {
        cleanDust();
        sanitize();
        moppedTheFloor();
    }

    @SneakyThrows
    private void cleanDust() {
        log.info("Cleaning the dust");
        Thread.sleep(taskTime);

    }

    @SneakyThrows
    private void sanitize() {
        log.info("Sanitizing");
        Thread.sleep(taskTime);

    }

    @SneakyThrows
    private void moppedTheFloor() {
        log.info("Mopping the flore");
        Thread.sleep(taskTime);
    }
}
