package com.example.office.service;

import com.example.office.Util.UserMapper;
import com.example.office.entity.User;
import com.example.office.exception.CustomException;
import com.example.office.exception.ExceptionMessages;
import com.example.office.model.request.LoginModelRequest;
import com.example.office.model.request.UserModelRequest;
import com.example.office.repository.UserRepository;
import com.example.office.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Objects;

@RequiredArgsConstructor
@Service
public class UserService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserMapper userMapper;

    public User addUser(UserModelRequest userInfo) {
        if (userExists(userInfo.getEmail())) {
            throw new CustomException(ExceptionMessages.USER_ALREADY_EXISTS.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
        User user = createUser(userInfo);
        return repository.save(user);
    }

    public String authenticate(LoginModelRequest loginInfo) {
        User user = repository.findByEmail(loginInfo.getEmail());
        if (Objects.isNull(user)) {
            throw new CustomException(ExceptionMessages.NO_USER_FOUND.getMessage(), HttpStatus.NOT_FOUND);
        }

        var authToken = new UsernamePasswordAuthenticationToken(loginInfo.getEmail(), loginInfo.getPassword());
        authenticationManager.authenticate(authToken);
        return jwtTokenProvider.createToken(loginInfo.getEmail(), Collections.singletonList(user.getUserRole()));
    }

    private User createUser(UserModelRequest userInfo) {
        User user = userMapper.mapModelRequestToEntity(userInfo);
        user.setEncryptedPassword(passwordEncoder.encode(userInfo.getPassword()));
        return user;
    }

    private boolean userExists(String email) {
        User user = repository.findByEmail(email);
        return Objects.nonNull(user);
    }
}
