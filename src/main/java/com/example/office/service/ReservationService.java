package com.example.office.service;

import com.example.office.Util.ReservationMapper;
import com.example.office.configuration.Properties;
import com.example.office.entity.Reservation;
import com.example.office.entity.User;
import com.example.office.exception.CustomException;
import com.example.office.exception.ExceptionMessages;
import com.example.office.model.response.ReservationRest;
import com.example.office.repository.ReservationRepository;
import com.example.office.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ReservationService {
    private final ReservationRepository reservationRepository;
    private final UserRepository userRepository;
    private final ReservationMapper mapper;

    private final Integer desksNumber = Properties.desksNumber;

    public List<ReservationRest> getAllReservations() {
        return convertToReservationRestList(reservationRepository.findAll());
    }

    public List<ReservationRest> getAllReservationsByDate(LocalDate date) {
        return convertToReservationRestList(reservationRepository.findAllByDate(date));
    }

    public List<ReservationRest> getAllReservationsByYear(Integer year) {
        Predicate<Reservation> filterCriteria = getFilterByYear(year);
        List<Reservation> reservations = filterReservations(filterCriteria);

        return convertToReservationRestList(reservations);
    }

    public List<ReservationRest> getAllReservationsByYearAndMonth(Integer year, Integer month) {
        Predicate<Reservation> filterCriteria = getFilterByYearAndMonth(year, month);
        List<Reservation> reservations = filterReservations(filterCriteria);

        return convertToReservationRestList(reservations);
    }

    public List<ReservationRest> getCurrentUserAllReservations() {
        User currentUser = getCurrentUser();
        return convertToReservationRestList(currentUser.getReservations());
    }

    public User addReservation(LocalDate date) {
        if (noDeskAvailable(date)) {
            throw new CustomException(ExceptionMessages.NO_DESK_AVAILABLE.getMessage()
                    + date.toString(), HttpStatus.OK);
        }

        User currentUser = getCurrentUser();
        if (userAlreadyHasReservation(currentUser, date)) {
            throw new CustomException(ExceptionMessages.ALREADY_HAS_RESERVATION.getMessage()
                    + date, HttpStatus.OK);
        }

        Reservation reservation = createReservation(currentUser, date);
        currentUser.getReservations().add(reservation);
        return userRepository.save(currentUser);
    }

    public User deleteReservation(LocalDate date) {
        User currentUser = getCurrentUser();
        if (userHasNoReservationInDate(currentUser, date)) {
            throw new CustomException(ExceptionMessages.NO_RESERVATION_FOR_DATE.getMessage()
                    + date, HttpStatus.NOT_FOUND);
        }
        currentUser.getReservations().removeIf(getFilterOnSameDate(date));
        userRepository.save(currentUser);
        return currentUser;
    }

    public Map<LocalDate, List<ReservationRest>> groupByDate() {
        return reservationRepository.findAll().stream()
                .map(mapper::convertToRest)
                .collect(Collectors.groupingBy(ReservationRest::getDate));
    }

    public Map<Month, Long> getNumberOfReservationsPerMonth(Integer year) {
        return reservationRepository.findAll().stream()
                .filter(getFilterByYear(year))
                .map(mapper::convertToRest)
                .collect(Collectors.groupingBy(reservation -> reservation.getDate().getMonth(),
                        Collectors.counting()));
    }

    private boolean userAlreadyHasReservation(User currentUser, LocalDate date) {
        return currentUser.getReservations().stream()
                .anyMatch(getFilterOnSameDate(date));
    }

    private Predicate<Reservation> getFilterByYearAndMonth(Integer year, Integer month) {
        return reservation -> {
            LocalDate date = reservation.getDate();
            return date.getYear() == year && date.getMonth() == Month.of(month);
        };
    }

    private Predicate<Reservation> getFilterByYear(Integer year) {
        return reservation -> reservation.getDate().getYear() == year;
    }

    private Predicate<Reservation> getFilterOnSameDate(LocalDate date) {
        return reservation -> reservation.getDate().isEqual(date);
    }

    private List<Reservation> filterReservations(Predicate<Reservation> filterCriteria) {
        return reservationRepository.findAll().stream()
                .filter(filterCriteria)
                .collect(Collectors.toList());
    }


    private List<ReservationRest> convertToReservationRestList(List<Reservation> reservations) {
        return reservations.stream()
                .map(mapper::convertToRest)
                .collect(Collectors.toList());
    }

    private boolean noDeskAvailable(LocalDate date) {
        return reservationRepository.findAllByDate(date).size() >= desksNumber;
    }

    private boolean userHasNoReservationInDate(User currentUser, LocalDate date) {
        return currentUser.getReservations().stream()
                .noneMatch(getFilterOnSameDate(date));
    }

    private Reservation createReservation(User currentUser, LocalDate date) {
        Reservation reservation = new Reservation();
        reservation.setDate(date);
        reservation.setUser(currentUser);
        return reservation;
    }

    private User getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String email = authentication.getName();
        return userRepository.findByEmail(email);
    }
}
