package com.example.office.entity;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    EMPLOYEE;

    @Override
    public String getAuthority() {
        return name();
    }
}
