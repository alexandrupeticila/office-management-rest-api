package com.example.office.security;

import com.example.office.entity.Role;
import com.example.office.entity.User;
import com.example.office.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class MyUserDetails implements UserDetailsService {

    private final UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = repository.findByEmail(username);
        if(user == null){
            throw new UsernameNotFoundException("User '" + username + "' not found");
        }

        String encryptedPassword = user.getEncryptedPassword();
        Role role = user.getUserRole();

        return org.springframework.security.core.userdetails.User//
                .withUsername(username)
                .password(encryptedPassword)
                .authorities(List.of(role))
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }
}
