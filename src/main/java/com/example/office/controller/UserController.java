package com.example.office.controller;

import com.example.office.model.request.LoginModelRequest;
import com.example.office.model.request.UserModelRequest;
import com.example.office.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/users")
@RestController
public class UserController {
    private final UserService service;

    @PostMapping("/add")
    public ResponseEntity<String> addEmployee(@RequestBody UserModelRequest userInfo) {
        service.addUser(userInfo);
        log.info("User created" + userInfo.toString() + ":" + LocalDateTime.now());
        return new ResponseEntity<>("Created!", HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public String singIn(@RequestBody LoginModelRequest loginInfo) {
        log.info("Trying to authenticate user");
        return service.authenticate(loginInfo);
    }
}
