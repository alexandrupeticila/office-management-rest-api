package com.example.office.controller;

import com.example.office.model.response.ReservationRest;
import com.example.office.service.ReservationService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@RequestMapping("/reservation")
@RestController
public class ReservationController {

    private final ReservationService service;

    @GetMapping("/all")
    public List<ReservationRest> getAllReservations() {
        return service.getAllReservations();
    }

   @GetMapping("/all/{date}")
    public List<ReservationRest> getAllReservationsByDate(@PathVariable("date")
                                                          @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        return service.getAllReservationsByDate(date);
    }

    @GetMapping("/{year}")
    public List<ReservationRest> getAllReservationsByYear(@PathVariable("year") Integer year) {
        return service.getAllReservationsByYear(year);
    }

    @GetMapping("/{year}/{month}")
    public List<ReservationRest> getAllReservationsByYearAndMonth(@PathVariable("year") Integer year,
                                                          @PathVariable("month") Integer month) {
        return service.getAllReservationsByYearAndMonth(year, month);
    }


    @GetMapping("/currentUser")
    public List<ReservationRest> getCurrentUserReservations() {
        return service.getCurrentUserAllReservations();
    }

    @PostMapping("/currentUser/{date}")
    public ResponseEntity<String> addCurrentUserReservation(@PathVariable("date")
                                                            @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        service.addReservation(date);
        return new ResponseEntity<>("Reservation successfully created!", HttpStatus.CREATED);
    }

    @DeleteMapping("/currentUser/{date}")
    public ResponseEntity<String> removeReservation(@PathVariable("date")
                                                    @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
        service.deleteReservation(date);
        return new ResponseEntity<>("Reservation successfully deleted!", HttpStatus.OK);
    }

    @GetMapping("/groupedByDate")
    public Map<LocalDate, List<ReservationRest>> groupByDate(){
        return service.groupByDate();
    }

    @GetMapping("/groupedByMonth/{year}")
    public Map<Month, Long> getNumberOfReservationsPerMonth(@PathVariable("year") Integer year){
        return service.getNumberOfReservationsPerMonth(year);
    }
}
