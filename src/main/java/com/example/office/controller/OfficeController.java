package com.example.office.controller;

import com.example.office.configuration.Properties;
import com.example.office.service.OfficeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/office")
@RestController
public class OfficeController {

    private final OfficeService service;

    @GetMapping("/desksNumber")
    public ResponseEntity<String> getNumberOfDesks() {
        Integer desksNumber = service.getDesksNumber();
        String status = String.format("The office has a number of %d desks", desksNumber);
        log.info(status);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @GetMapping("/cleaning/{workersNumber}")
    public ResponseEntity<String> cleanDesks(@PathVariable("workersNumber") Integer workersNumber) {
        long cleaningTime = service.doCleaning(workersNumber);
        log.info(String.valueOf(cleaningTime));
        return new ResponseEntity<>("Successfully cleaned in: " + cleaningTime, HttpStatus.OK);
    }

    @GetMapping("/splitCleaning")
    public ResponseEntity<String> splitCleaning() {
        long cleaningTime = service.splitCleaning();
        log.info(String.valueOf(cleaningTime));
        return new ResponseEntity<>("Successfully cleaned in: " + cleaningTime, HttpStatus.OK);
    }
}
