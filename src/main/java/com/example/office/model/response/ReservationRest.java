package com.example.office.model.response;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ReservationRest {
    private String userEmail;
    private String userFirstName;
    private String userLastName;
    private LocalDate date;
}
