package com.example.office.model.request;

import com.example.office.entity.Role;
import lombok.Data;

@Data
public class UserModelRequest {

    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private Role userRole;
}
