package com.example.office.model.request;

import lombok.Data;

@Data
public class LoginModelRequest {
    private String email;
    private String password;
}
