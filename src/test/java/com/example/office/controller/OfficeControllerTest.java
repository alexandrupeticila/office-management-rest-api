package com.example.office.controller;

import com.example.office.service.OfficeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@ExtendWith(MockitoExtension.class)
class OfficeControllerTest {

    @Mock
    private OfficeService service;

    @InjectMocks
    private OfficeController controller;

    @Test
    void getNumberOfDesks(){
        //Given
        when(service.getDesksNumber()).thenReturn(10);
        //When
        ResponseEntity<String> response = controller.getNumberOfDesks();
        //Then
        assertTrue(response.getBody().contains("10"));
    }

    @Test
    void cleanDesks() {
        //Given
        int workersNumber = 10;
        when(service.doCleaning(workersNumber)).thenReturn(150L);
        //When
        ResponseEntity<String> response = controller.cleanDesks(workersNumber);
        //Then
        assertTrue(response.getBody().contains("Successfully cleaned"));
        assertTrue(response.getBody().contains("150"));
    }

    @Test
    void splitCleaning() {
        //Given
        when(service.splitCleaning()).thenReturn(150L);
        //When
        ResponseEntity<String> response = controller.splitCleaning();
        //Then
        assertTrue(response.getBody().contains("Successfully cleaned"));
        assertTrue(response.getBody().contains("150"));
    }
}