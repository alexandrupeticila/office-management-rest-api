package com.example.office.controller;

import com.example.office.entity.Role;
import com.example.office.entity.User;
import com.example.office.model.request.LoginModelRequest;
import com.example.office.model.request.UserModelRequest;
import com.example.office.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    private UserService service;

    @InjectMocks
    private UserController controller;


    @Test
    void addEmployee() {
        //Given
        var userModelRequest = generateUserModelRequest();
        when(service.addUser(any())).thenReturn(any());
        //When
        var result = controller.addEmployee(userModelRequest);
        //Then
        assertTrue(result.getBody().contains("Created!"));
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    void singIn() {
        //Given
        String token = "jwt";
        var loginModelRequest = generateLoginModelRequest();
        when(service.authenticate(loginModelRequest)).thenReturn(token);
        //When
        var result = controller.singIn(loginModelRequest);
        //Then
        assertEquals(token, result);
    }

    private UserModelRequest generateUserModelRequest() {
        UserModelRequest userModelRequest = new UserModelRequest();
        userModelRequest.setEmail("alexandru.peticila@gmail.com");
        userModelRequest.setFirstName("Alexandru");
        userModelRequest.setLastName("Peticila");
        userModelRequest.setPassword("root1234");
        userModelRequest.setUserRole(Role.EMPLOYEE);
        return userModelRequest;
    }

    private LoginModelRequest generateLoginModelRequest() {
        var loginInfo = new LoginModelRequest();
        loginInfo.setEmail("alexandru.peticila@gmail.com");
        loginInfo.setPassword("root1234");
        return loginInfo;
    }
}