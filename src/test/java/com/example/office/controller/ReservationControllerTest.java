package com.example.office.controller;

import com.example.office.entity.Reservation;
import com.example.office.model.response.ReservationRest;
import com.example.office.service.ReservationService;
import com.example.office.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReservationControllerTest {

    @Mock
    private ReservationService reservationService;

    @InjectMocks
    private ReservationController reservationController;

    @Test
    void getAllReservations() {
        //Given
        var reservations = generateReservations();
        when(reservationService.getAllReservations()).thenReturn(reservations);
        //When
        var result = reservationController.getAllReservations();
        //Then
        assertEquals(reservations.size(), result.size());
        assertEquals(reservations, result);
    }

    @Test
    void getAllReservationsByDate() {
        //Given
        var reservations = generateReservations();
        LocalDate date = LocalDate.of(2021, 2, 24);
        when(reservationService.getAllReservationsByDate(any())).thenReturn(reservations);
        //When
        var result = reservationController.getAllReservationsByDate(date);
        //Then
        assertEquals(reservations.size(), result.size());
        assertEquals(reservations, result);
    }

    @Test
    void getAllReservationsByYear() {
        //Given
        var reservations = generateReservations();
        Integer year = 2021;
        when(reservationService.getAllReservationsByYear(year)).thenReturn(reservations);
        //When
        var result = reservationController.getAllReservationsByYear(year);
        //Then
        assertEquals(reservations.size(), result.size());
        assertEquals(reservations, result);
    }
    @Test
    void getAllReservationsByYearAndMonth() {
        //Given
        var reservations = generateReservations();
        Integer year = 2021;
        Integer month = 2;
        when(reservationService.getAllReservationsByYearAndMonth(year, month)).thenReturn(reservations);
        //When
        var result = reservationController.getAllReservationsByYearAndMonth(year, month);
        //Then
        assertEquals(reservations.size(), result.size());
        assertEquals(reservations, result);
    }


    @Test
    void getCurrentUserReservations() {
        //Given
        var reservations = generateReservations();
        when(reservationService.getCurrentUserAllReservations()).thenReturn(reservations);
        //When
        var result = reservationController.getCurrentUserReservations();
        //Then
        assertEquals(reservations.size(), result.size());
        assertEquals(reservations, result);
    }

    @Test
    void addCurrentUserReservation() {
        //Given
        LocalDate date = LocalDate.of(2021, 2, 24);
        when(reservationService.addReservation(date)).thenReturn(any());
        //When
        var result = reservationController.addCurrentUserReservation(date);
        //Then
        assertNotEquals(null, result.getBody() );
        assertTrue(result.getBody().contains("Reservation successfully created!"));
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    void removeReservation() {
        //Given
        LocalDate date = LocalDate.of(2021, 2, 24);
        when(reservationService.deleteReservation(date)).thenReturn(any());
        //When
        var result = reservationController.removeReservation(date);
        //Then
        assertNotEquals(null, result.getBody() );
        assertTrue(result.getBody().contains("Reservation successfully deleted!"));
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    void groupByDate() {
        //Given
        LocalDate date = LocalDate.of(2021, 2, 24);
        var map = Map.of(date, generateReservations());
        when(reservationService.groupByDate()).thenReturn(map);
        //When
        var result = reservationController.groupByDate();
        //Then
        assertEquals(map.size(), result.size());
        assertEquals(map, result);
    }

    @Test
    void getNumberOfReservationsPerMonth() {
        //Given
        Map<Month, Long> map = Map.of(Month.FEBRUARY,2L, Month.APRIL, 3L);
        when(reservationService.getNumberOfReservationsPerMonth(any())).thenReturn(map);
        //When
        var result = reservationController.getNumberOfReservationsPerMonth(2021);
        //Then
        assertEquals(map.size(), result.size());
        assertEquals(map, result);
    }

    private List<ReservationRest> generateReservations(){
        return List.of(generateReservationRest());
    }

    private ReservationRest generateReservationRest() {
        ReservationRest reservationRest = new ReservationRest();
        reservationRest.setDate(LocalDate.of(2021, 2,24));
        reservationRest.setUserEmail("alexandru.peticila@gmail.com");
        reservationRest.setUserFirstName("Alexandru");
        reservationRest.setUserLastName("Peticila");
        return reservationRest;
    }

}