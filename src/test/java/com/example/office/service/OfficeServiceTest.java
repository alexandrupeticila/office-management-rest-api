package com.example.office.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertTrue;

class OfficeServiceTest {

    private final OfficeService officeService = new OfficeService();

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 7, 10})
    void doCleaning(int workersNumber) {
        //Given
        int desksNumber = 10;
        int taskTime = 5;
        ReflectionTestUtils.setField(officeService, "desksNumber", desksNumber);
        ReflectionTestUtils.setField(officeService, "taskTime", taskTime);
        //When
        long totalTime = officeService.doCleaning(workersNumber);
        //Then
        assertTrue((desksNumber * taskTime) / (3L * workersNumber) <= totalTime);
    }

    @Test
    void splitCleaning() {
        //Given
        int desksNumber = 10;
        int taskTime = 5;
        ReflectionTestUtils.setField(officeService, "desksNumber", desksNumber);
        ReflectionTestUtils.setField(officeService, "taskTime", taskTime);
        //When
        long totalTime = officeService.splitCleaning();
        //Then
        assertTrue(desksNumber * taskTime <= totalTime);
        assertTrue(desksNumber * totalTime * 2 > totalTime);
    }
}