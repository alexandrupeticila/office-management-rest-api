package com.example.office.service;

import com.example.office.Util.UserMapper;
import com.example.office.entity.Role;
import com.example.office.entity.User;
import com.example.office.exception.CustomException;
import com.example.office.exception.ExceptionMessages;
import com.example.office.model.request.LoginModelRequest;
import com.example.office.model.request.UserModelRequest;
import com.example.office.repository.UserRepository;
import com.example.office.security.JwtTokenProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder encoder;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private JwtTokenProvider jwtTokenProvider;
    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserService userService;

    @Test
    void addUser() {
        //Given
        UserModelRequest userInfo = generateUserModelRequest();
        User user = generateUser(userInfo);
        when(userRepository.findByEmail(userInfo.getEmail())).thenReturn(null);
        when(userMapper.mapModelRequestToEntity(userInfo)).thenReturn(user);
        when(encoder.encode(userInfo.getPassword())).thenReturn(userInfo.getPassword());
        when(userRepository.save(user)).thenReturn(user);
        //When
        User createdUser = userService.addUser(userInfo);
        //Then
        assertEquals(user, createdUser);
        assertEquals(user.getEncryptedPassword() ,createdUser.getEncryptedPassword());
        assertTrue(createdUser.toString().contains(user.getEmail()));
    }

    @Test
    void addUser_UserAlreadyExists() {
        //Given
        UserModelRequest userInfo = generateUserModelRequest();
        User user = generateUser(userInfo);
        when(userRepository.findByEmail(userInfo.getEmail())).thenReturn(user);
        //When
        CustomException customException = assertThrows(CustomException.class, () ->
                userService.addUser(userInfo));
        //Then
        assertEquals(ExceptionMessages.USER_ALREADY_EXISTS.getMessage(), customException.getMessage());
    }

    @Test
    void authenticate() {
        //Given
        var loginInfo = generateLoginModelRequest();
        User user = generateUser(generateUserModelRequest());
        String jwt = "jwt";
        when(userRepository.findByEmail(loginInfo.getEmail())).thenReturn(user);
        when(authenticationManager.authenticate(any())).thenReturn(null);
        when(jwtTokenProvider.createToken(loginInfo.getEmail(), Collections.singletonList(user.getUserRole())))
                .thenReturn(jwt);
        //When
        String jwtToken = userService.authenticate(loginInfo);
        //Then
        assertEquals(jwt, jwtToken);
    }

    @Test
    void authenticate_NoUser() {
        //Given
        var loginInfo = generateLoginModelRequest();
        when(userRepository.findByEmail(loginInfo.getEmail())).thenReturn(null);
        //When
        CustomException customException = assertThrows(CustomException.class, () ->
                userService.authenticate(loginInfo));
        //Then
        assertEquals(ExceptionMessages.NO_USER_FOUND.getMessage(), customException.getMessage());
        assertEquals(HttpStatus.NOT_FOUND, customException.getHttpStatus());
    }

    private UserModelRequest generateUserModelRequest() {
        UserModelRequest userModelRequest = new UserModelRequest();
        userModelRequest.setEmail("alexandru.peticila@gmail.com");
        userModelRequest.setFirstName("Alexandru");
        userModelRequest.setLastName("Peticila");
        userModelRequest.setPassword("root1234");
        userModelRequest.setUserRole(Role.EMPLOYEE);
        return userModelRequest;
    }

    private User generateUser(UserModelRequest userInfo) {
        User user = new User();
        user.setEmail(userInfo.getEmail());
        user.setFirstName(userInfo.getFirstName());
        user.setLastName(userInfo.getLastName());
        user.setEncryptedPassword(userInfo.getPassword());
        return user;
    }

    private LoginModelRequest generateLoginModelRequest() {
        var loginInfo = new LoginModelRequest();
        loginInfo.setEmail("alexandru.peticila@gmail.com");
        loginInfo.setPassword("root1234");
        return loginInfo;
    }
}