package com.example.office.service;

import com.example.office.Util.ReservationMapper;
import com.example.office.entity.Reservation;
import com.example.office.entity.User;
import com.example.office.exception.CustomException;
import com.example.office.exception.ExceptionMessages;
import com.example.office.model.response.ReservationRest;
import com.example.office.repository.ReservationRepository;
import com.example.office.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReservationServiceTest {

    @Mock
    private ReservationMapper mapper;
    @Mock
    private ReservationRepository reservationRepository;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private ReservationService reservationService;

    @Test
    void getAllReservations() {
        //Given
        Reservation reservation = generateReservation();
        ReservationRest reservationRest = generateReservationRest(reservation);
        when(reservationRepository.findAll()).thenReturn(List.of(reservation));
        when(mapper.convertToRest(reservation)).thenReturn(reservationRest);
        //When
        List<ReservationRest> allReservations = reservationService.getAllReservations();
        //Then
        ReservationRest result = allReservations.get(0);
        assertEquals(1, allReservations.size());
        assertEquals(reservationRest.getDate(), result.getDate());
        assertEquals(reservationRest.getUserEmail(), result.getUserEmail());
        assertEquals(reservationRest.getUserFirstName(), result.getUserFirstName());
    }

    @Test
    void getAllReservationsByDate() {
        //Given
        Reservation reservation = generateReservation();
        ReservationRest reservationRest = generateReservationRest(reservation);
        when(reservationRepository.findAllByDate(any())).thenReturn(List.of(reservation));
        when(mapper.convertToRest(reservation)).thenReturn(reservationRest);
        LocalDate date = LocalDate.of(2021, 2, 24);
        //When
        List<ReservationRest> allReservations = reservationService.getAllReservationsByDate(date);
        //Then
        ReservationRest result = allReservations.get(0);
        assertEquals(1, allReservations.size());
        assertEquals(reservationRest.getDate(), result.getDate());
        assertEquals(reservationRest.getUserEmail(), result.getUserEmail());
        assertEquals(reservationRest.getUserFirstName(), result.getUserFirstName());
        assertEquals(reservationRest.getUserLastName(), result.getUserLastName());
    }

    @Test
    void getAllReservationsByYear() {
        //Given
        Reservation reservation = generateReservation();
        ReservationRest reservationRest = generateReservationRest(reservation);
        when(reservationRepository.findAll()).thenReturn(List.of(reservation));
        when(mapper.convertToRest(reservation)).thenReturn(reservationRest);
        Integer year = 2021;
        //When
        List<ReservationRest> allReservations = reservationService.getAllReservationsByYear(year);
        //Then
        ReservationRest result = allReservations.get(0);
        assertEquals(1, allReservations.size());
        assertEquals(reservationRest.getDate(), result.getDate());
        assertEquals(reservationRest.getUserEmail(), result.getUserEmail());
        assertEquals(reservationRest.getUserFirstName(), result.getUserFirstName());
    }

    @Test
    void getAllReservationsByYearAndMonth() {
        //Given
        Reservation reservation = generateReservation();
        ReservationRest reservationRest = generateReservationRest(reservation);
        when(reservationRepository.findAll()).thenReturn(List.of(reservation));
        when(mapper.convertToRest(reservation)).thenReturn(reservationRest);
        Integer year = 2021;
        Integer month = 2;
        //When
        List<ReservationRest> allReservations = reservationService.getAllReservationsByYearAndMonth(year, month);
        //Then
        ReservationRest result = allReservations.get(0);
        assertEquals(1, allReservations.size());
        assertEquals(reservationRest.getDate(), result.getDate());
        assertEquals(reservationRest.getUserEmail(), result.getUserEmail());
        assertEquals(reservationRest.getUserFirstName(), result.getUserFirstName());
    }

    @Test
    void getCurrentUserAllReservations() {
        //Given
        Reservation reservation = generateReservation();
        User user = generateUser();
        user.setReservations(List.of(reservation));
        initializeAuthentication();
        ReservationRest reservationRest = generateReservationRest(reservation);
        when(userRepository.findByEmail(any())).thenReturn(user);
        when(mapper.convertToRest(reservation)).thenReturn(reservationRest);
        //When
        List<ReservationRest> allReservations = reservationService.getCurrentUserAllReservations();
        //Then
        assertEquals(1, allReservations.size());
        ReservationRest result = allReservations.get(0);
        assertEquals(reservationRest.getDate(), result.getDate());
        assertEquals(reservationRest.getUserEmail(), result.getUserEmail());
        assertEquals(reservationRest.getUserFirstName(), result.getUserFirstName());
    }

    @Test
    void addReservation() {
        //Given
        initializeAuthentication();
        User user = generateUser();
        Reservation reservation = generateReservation();
        LocalDate date = LocalDate.of(2021, 2, 24);
        ReflectionTestUtils.setField(reservationService, "desksNumber", 10);

        when(reservationRepository.findAllByDate(any())).thenReturn(Collections.emptyList());
        when(userRepository.findByEmail(any())).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);
        //When
        User updatedUser = reservationService.addReservation(date);
        //Then
        var reservations = updatedUser.getReservations();
        assertEquals(1, reservations.size());
        assertEquals(reservation, reservations.get(0));
        assertEquals(date, reservation.getDate());
        assertTrue(reservations.get(0).toString().contains("userEmail =" + updatedUser.getEmail()));
    }

    @Test
    void addReservation_NoDeskAvailable() {
        //Given
        LocalDate date = LocalDate.of(2021, 2, 24);
        ReflectionTestUtils.setField(reservationService, "desksNumber", 0);
        when(reservationRepository.findAllByDate(any())).thenReturn(Collections.emptyList());
        //When
        CustomException customException = assertThrows(CustomException.class, () ->
                reservationService.addReservation(date));
        //Then
        assertEquals(ExceptionMessages.NO_DESK_AVAILABLE.getMessage() + date.toString(),
                customException.getMessage());
    }

    @Test
    void addReservation_userAlreadyHasReservation() {
        //Given
        initializeAuthentication();
        User user = generateUser();
        user.setReservations(List.of(generateReservation()));
        LocalDate date = LocalDate.of(2021, 2, 24);
        ReflectionTestUtils.setField(reservationService, "desksNumber", 10);

        when(userRepository.findByEmail(any())).thenReturn(user);
        when(reservationRepository.findAllByDate(any())).thenReturn(Collections.emptyList());
        //When
        CustomException customException = assertThrows(CustomException.class, () ->
                reservationService.addReservation(date));
        //Then
        assertEquals(ExceptionMessages.ALREADY_HAS_RESERVATION.getMessage() + date.toString(),
                customException.getMessage());
    }

    @Test
    void deleteReservation() {
        //Given
        initializeAuthentication();
        User user = generateUser();
        var reservations = Stream.of(generateReservation()).collect(Collectors.toList());
        user.setReservations(reservations);
        LocalDate date = LocalDate.of(2021, 2, 24);

        when(userRepository.findByEmail(any())).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);
        //When
        User updatedUser = reservationService.deleteReservation(date);
        //Then
        var updatedReservations = updatedUser.getReservations();
        assertEquals(0, updatedReservations.size());
    }

    @Test
    void deleteReservation_UserHasNoReservation() {
        //Given
        initializeAuthentication();
        User user = generateUser();
        LocalDate date = LocalDate.of(2021, 2, 24);

        when(userRepository.findByEmail(any())).thenReturn(user);
        //When
        CustomException customException = assertThrows(CustomException.class, () ->
                reservationService.deleteReservation(date));

        assertEquals(ExceptionMessages.NO_RESERVATION_FOR_DATE.getMessage() + date.toString(),
                customException.getMessage());
    }

    @Test
    void groupByDate() {
        //Given
        Reservation reservation = generateReservation();
        ReservationRest reservationRest = generateReservationRest(reservation);
        when(reservationRepository.findAll()).thenReturn(List.of(reservation));
        when(mapper.convertToRest(reservation)).thenReturn(reservationRest);
        LocalDate date = LocalDate.of(2021, 2, 24);
        //When
        Map<LocalDate, List<ReservationRest>> result = reservationService.groupByDate();
        //Then
        assertEquals(1, result.size());
        assertTrue(result.containsKey(date));
        assertEquals(1, result.get(date).size());
    }

    @Test
    void getNumberOfReservationsPerMonth() {
        //Given
        Reservation reservation = generateReservation();
        ReservationRest reservationRest = generateReservationRest(reservation);
        when(reservationRepository.findAll()).thenReturn(List.of(reservation));
        when(mapper.convertToRest(reservation)).thenReturn(reservationRest);
        Integer year = 2021;
        //When
        Map<Month, Long> result = reservationService.getNumberOfReservationsPerMonth(year);
        //Then
        assertEquals(1, result.get(Month.FEBRUARY));
        assertEquals(1, result.size());
        assertTrue(result.containsKey(Month.FEBRUARY));
    }

    private void initializeAuthentication() {
        Authentication authentication = Mockito.mock(Authentication.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);

        when(authentication.getName()).thenReturn(generateUser().getEmail());
        SecurityContextHolder.setContext(securityContext);
    }


    private ReservationRest generateReservationRest(Reservation reservation) {
        ReservationRest reservationRest = new ReservationRest();
        reservationRest.setDate(reservation.getDate());
        reservationRest.setUserEmail(reservation.getUser().getEmail());
        reservationRest.setUserFirstName(reservation.getUser().getFirstName());
        reservationRest.setUserLastName(reservation.getUser().getLastName());
        return reservationRest;
    }

    private Reservation generateReservation() {
        Reservation reservation1 = new Reservation();
        reservation1.setId(1);
        reservation1.setDate(LocalDate.of(2021, 2, 24));
        reservation1.setUser(generateUser());
        return reservation1;
    }

    private User generateUser() {
        User user = new User();
        user.setEmail("alexandru.peticila@yahoo.com");
        user.setFirstName("Alexandru");
        user.setLastName("Peticila");
        return user;
    }
}