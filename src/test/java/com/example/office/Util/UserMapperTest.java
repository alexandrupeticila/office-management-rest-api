package com.example.office.Util;

import com.example.office.entity.Role;
import com.example.office.entity.User;
import com.example.office.model.request.UserModelRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserMapperTest {

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private UserMapper userMapper;

    @Test
    void mapModelRequestToEntity() {
        //Given
        var userModelRequest = generateUserModelRequest();
        var user = generateUser(userModelRequest);
        when(modelMapper.map(userModelRequest, User.class)).thenReturn(user);
        //When
        User mappedUser = userMapper.mapModelRequestToEntity(userModelRequest);
        // Then
        assertEquals(user, mappedUser);
        assertEquals("EMPLOYEE", mappedUser.getUserRole().getAuthority());
    }

    private UserModelRequest generateUserModelRequest() {
        UserModelRequest userModelRequest = new UserModelRequest();
        userModelRequest.setEmail("alexandru.peticila@gmail.com");
        userModelRequest.setFirstName("Alexandru");
        userModelRequest.setLastName("Peticila");
        userModelRequest.setPassword("root1234");
        userModelRequest.setUserRole(Role.EMPLOYEE);
        return userModelRequest;
    }

    private User generateUser(UserModelRequest userInfo) {
        User user = new User();
        user.setEmail(userInfo.getEmail());
        user.setFirstName(userInfo.getFirstName());
        user.setLastName(userInfo.getLastName());
        user.setUserRole(userInfo.getUserRole());
        return user;
    }
}