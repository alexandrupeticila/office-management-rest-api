package com.example.office.Util;

import com.example.office.entity.Reservation;
import com.example.office.entity.User;
import com.example.office.model.response.ReservationRest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReservationMapperTest {

    @Mock
    private ModelMapper mapper;

    @InjectMocks
    private ReservationMapper reservationMapper;

    @Test
    void convertToRest() {
        //Given
        Reservation reservation = generateReservation();
        ReservationRest reservationRest = generateReservationRest(reservation);
        when(mapper.map(reservation, ReservationRest.class)).thenReturn(reservationRest);
        //When
        ReservationRest result = reservationMapper.convertToRest(reservation);
        //Then
        assertEquals(reservationRest, result);
    }

    private ReservationRest generateReservationRest(Reservation reservation) {
        ReservationRest reservationRest = new ReservationRest();
        reservationRest.setDate(reservation.getDate());
        reservationRest.setUserEmail(reservation.getUser().getEmail());
        reservationRest.setUserEmail(reservation.getUser().getFirstName());
        reservationRest.setUserLastName(reservation.getUser().getLastName());
        return reservationRest;
    }

    private Reservation generateReservation() {
        Reservation reservation1 = new Reservation();
        reservation1.setId(1);
        reservation1.setDate(LocalDate.of(2021, 2, 24));
        reservation1.setUser(generateUser());
        return reservation1;
    }

    private User generateUser() {
        User user = new User();
        user.setEmail("alexandru.peticila@yahoo.com");
        user.setFirstName("Alexandru");
        user.setLastName("Peticila");
        return user;
    }
}